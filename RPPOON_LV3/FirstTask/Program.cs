﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstTask
{
    class Program
    {
        //Pitanje: Je li potrebno duboko kopiranje za klasu u pitanju? 
        //Odgovor: Duboko kopiranje u ovoj klasi je potrebno zato što imamo listu od lista (list of lists) pa duboko kopiranje kopira ugnježđenu listu također, 
        //dok bi s plitkim kopiranjem kopirali vanjsku listu, a unutra bi bile reference.
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("C:\\Users\\Kivi\\source\\repos\\RPPOON_LV3\\File.csv"); 
            Console.WriteLine("Data:");
            dataset.DisplayData();

            Dataset datasetDeep = (Dataset)dataset.Clone();
            Console.WriteLine("Deep data:");
            datasetDeep.DisplayData();
        }
    }
}


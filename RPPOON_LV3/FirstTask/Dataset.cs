﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstTask
{
    class Dataset : Prototype
    {
        private List<List<string>> data; 

        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return  new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }
        public Prototype Clone()
        {
            Dataset result = (Dataset)this.MemberwiseClone();
            
            result.data = new List<List<string>>(this.data.Count);
            foreach (List<string> list in this.data)
            {
                List<string> row = new List<string>();
                foreach (string entry in list)
                {
                    row.Add((string)entry.Clone());
                }
                result.data.Add(row);
            }
            return result;
        }
        public void DisplayData()
        {
            foreach (List<string> list in GetData())
            {
                foreach (string entry in list)
                {
                    Console.Write(entry);
                    Console.Write((entry != list.Last()) ? "," : "\n");
                }
            }
            Console.WriteLine();
        }
    }
}


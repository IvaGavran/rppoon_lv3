﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecondTask
{
    class MatrixGenerator
    {
        private static MatrixGenerator instance;
        private Random generator;

        private MatrixGenerator()
        {
            this.generator = new Random();
        }
        public static MatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new MatrixGenerator();
            }
            return instance;
        }
        public double[][] CreateRandomMatrix(int rows, int columns)
        {
            double[][] matrix = new double[rows][];
            List<double> list = new List<double>();

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    list.Add(this.generator.NextDouble());
                }
                matrix[i] =list.ToArray();
                list.Clear();
            }
            return matrix;
        }
    }
}

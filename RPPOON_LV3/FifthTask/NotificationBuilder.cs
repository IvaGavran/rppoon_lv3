﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FifthTask
{
    class NotificationBuilder : IBuilder
    {
        String author = String.Empty;
        String title = String.Empty;
        String text = String.Empty;
        DateTime time = new DateTime();
        Category level = Category.ERROR;
        ConsoleColor color = Console.ForegroundColor;

        public NotificationBuilder() { }
        public ConsoleNotification Build()
        {
            return new ConsoleNotification(author, title, text, time, level, color);
        }
        public IBuilder SetAuthor(String author) { this.author = author; return this; }
        public IBuilder SetTitle(String title) { this.title = title; return this; }
        public IBuilder SetTime(DateTime time) { this.time = time; return this; }
        public IBuilder SetLevel(Category level) { this.level = level; return this; }
        public IBuilder SetColor(ConsoleColor color) { this.color = color; return this; }
        public IBuilder SetText(String text) { this.text = text; return this; }
    }
}
